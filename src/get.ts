import { INumberMap, IMap } from "./iMap.type";

export function get<T>(map: IMap<T>, key: string): T;
export function get<T>(map: INumberMap<T>, key: number): T;
export function get<T>(map: IMap<T> | INumberMap<T>, key: string | number): T {
  if (typeof map.hasOwnProperty === "function" && !map.hasOwnProperty(key)) {
    return undefined;
  }

  return map[key];
}

