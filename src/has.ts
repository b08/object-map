import { IMap, INumberMap } from "./iMap.type";

export function has<TVal>(map: IMap<TVal>, key: string): boolean;
export function has<TVal>(map: INumberMap<TVal>, key: number): boolean;
export function has<TVal>(map: IMap<TVal>, key: string | number): boolean {
  return typeof map.hasOwnProperty === "function"
    ? map.hasOwnProperty(key)
    : map[key] !== undefined;
}
