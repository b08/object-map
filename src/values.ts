import { IMap, INumberMap } from "./iMap.type";

export function values<TVal = any>(map: IMap<TVal>): TVal[];
export function values<TVal = any>(map: INumberMap<TVal>): TVal[];
export function values<TVal>(map: IMap<TVal>): TVal[] {
  return Object.values(map);
}
