import { IMap, INumberMap } from "./iMap.type";
import { forIn } from "./forIn";

export function keys<TVal>(map: IMap<TVal>): string[] {
  return Object.keys(map);
}

export function nkeys<TVal>(map: INumberMap<TVal>): number[] {
  const result: number[] = [];
  forIn(map, (value, key) => result.push(+key));
  return result;
}
