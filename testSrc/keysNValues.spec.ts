import { test } from "@b08/test-runner";
import { keys, values, nkeys, extend } from "../src";

test("keys should return list of keys", async t => {
  // arrange
  const src = { "1": 2, "2": 3 };
  const expected = ["1", "2"];

  // act
  const result = keys(src);

  // assert
  t.deepEqual(result, expected);
});

test("nkeys should return list of number keys", async t => {
  // arrange
  const src = { 1: 2, 2: 3 };
  const expected = [1, 2];

  // act
  const result = nkeys(src);

  // assert
  t.deepEqual(result, expected);
});

test("keys should return list of null object keys", async t => {
  // arrange
  const src = extend({ "1": 2, "2": 3 });
  const expected = ["1", "2"];

  // act
  const result = keys(src);

  // assert
  t.deepEqual(result, expected);
});

test("nkeys should return list of null object number keys", async t => {
  // arrange
  const src = extend({ 1: 2, 2: 3 });
  const expected = [1, 2];

  // act
  const result = nkeys(src);

  // assert
  t.deepEqual(result, expected);
});


test("values should list of values", async t => {
  // arrange
  const src = { "1": 2, "2": 3 };
  const expected = [2, 3];

  // act
  const result = values(src);

  // assert
  t.deepEqual(result, expected);
});
