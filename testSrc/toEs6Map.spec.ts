import { test } from "@b08/test-runner";
import { toEs6Map } from "../src";

test("toEs6Map should convert objectMap to Es6 Map", async t => {
  // arrange
  const src = {
    "1": "2",
    "2": "3"
  };

  // act
  const result = toEs6Map(src);

  // assert
  t.equal(result.constructor, Map);
  t.equal(result.get("1"), "2");
  t.equal(result.get("2"), "3");
  t.false(result.has("3"));
});
