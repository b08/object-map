import { test } from "@b08/test-runner";
import { has, extend } from "../src";
import { constructor } from "./constructorName";

test("has should tell if field is present on regular object", async t => {
  // arrange
  const src = { "a": "b" };

  // act

  // assert
  t.true(has(src, "a"));
  t.false(has(src, "b"));
  t.false(has(src, constructor));
});

test("has should tell if field is present on null object", async t => {
  // arrange
  const src = extend({ "a": "b" });

  // act

  // assert
  t.true(has(src, "a"));
  t.false(has(src, "b"));
  t.false(has(src, constructor));
});
