import { test } from "@b08/test-runner";
import { extend, get } from "../src";
import { constructor } from "./constructorName";

test("get should return values from map", async t => {
  // arrange
  const src = {
    "a": true,
    "b": true
  };

  // act
  // assert
  t.true(get(src, "a"));
  t.true(src["b"]);
  t.true(get(src, "c") == null);
  t.true(get(src, constructor) == null);
});

test("get should return values from extended map", async t => {
  // arrange
  const src = extend({
    "a": true,
    "b": true
  });

  // act
  // assert
  t.true(get(src, "a"));
  t.true(src["b"]);
  t.true(get(src, "c") == null);
  t.true(get(src, constructor) == null);
});
