import { test } from "@b08/test-runner";
import { toMap } from "../src";
import { constructor } from "./constructorName";

test("toMap should return IMap with needed values", async t => {
  // arrange
  const src = [{ id: 1 }, { id: 2 }];

  // act
  const result = toMap(src, item => item.id);

  // assert
  t.equal(result[1], src[0]);
  t.equal(result[2], src[1]);
  t.true(result[3] == null);
});

test("toMap should not throw for null", async t => {
  // act
  toMap<number>(null, item => item);
});

test("should return IMap with flags", async t => {
  // arrange
  const src = ["a", "b"];

  // act
  const result = toMap(src, s => s, () => true);

  // assert
  t.true(result["a"]);
  t.true(result["b"]);
  t.true(null == result["c"]);
  t.true(null == result[constructor]);
});
