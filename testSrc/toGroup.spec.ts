import { test } from "@b08/test-runner";
import { toGroup } from "../src";
import { constructor } from "./constructorName";

test("toGroup should return ILookup with needed arrays", async t => {
  // arrange
  const src = [{ type: "list" }, { type: "list" }, { type: "item" }];

  // act
  const map = toGroup(src, item => item.type, item => item);

  // assert
  t.deepEqual(map["list"], [src[0], src[1]]);
  t.deepEqual(map["item"], [src[2]]);
  t.true(null == map[constructor]);
});

test("toGroup should work with default selector", async t => {
  // arrange
  const src = [{ type: "list" }, { type: "list" }, { type: "item" }];

  // act
  const map = toGroup(src, item => item.type);

  // assert
  t.deepEqual(map["list"], [src[0], src[1]]);
  t.deepEqual(map["item"], [src[2]]);
  t.true(null == map[constructor]);
});

test("toGroup should not throw for null", async t => {
  // act
  toGroup<number>(null, item => item);
});
