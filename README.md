# @b08/object-map, seeded from @b08/library-seed, library type: dry
Set of methods to work with null-prototyped object as map

# reasoning
Main purpose of using object as hash map is to have a fast, O(1) search for items using given key/id. \
Toolset is made of few, most frequently used functions to create and work with those maps.

Reason to use null-prototyped object (null-object from now on) is to not have properties derived from usual "Object" prototype, like "constructor".
Null-object will return undefined if you request "constructor" field by indexer.

# limitations
1. There is still that special property  "\_\_proto\_\_" that will still return nun-undefined value.
2. Null-object will not survive object-spread operator or serialization - deserialization. I.e. will become regular object.


# toMap
Returns null-object where keys are selected by first lambda and values are selected by second lambda. 
If key is encountered second time, value is overwritten.
```
const array = [{id: 1, name: '1'}, {id: 2, name '2'}];

const result = toMap(array, item => item.id); // {1: {id: 1, name: '1'}, 2: {id: 2, name '2'}}
const result2 = toMap(array, item => item.id, item => item.name); //{ 1: "1", 2: "2" }
```

# toGroup
Returns null-object, each entry is a key and array of values having that key.
```
const array = [{id: 1, name: '1'}, {id: 2, name '2'}, {id: 1, name: '3'}];

const result = toGroup(array, item => item.id); 
  // {1: [ {id: 1, name: '1'}, {id: 1, name: '3'}] 2: [{id: 2, name '2'}]}
const result2 = toGroup(array, item => item.id, item => item.name); //{ 1: ["1", "3"], 2: ["2"] }
```

# keys and values
keys and values return all keys and values respectively, support both regular object and null-object.

# get and has
By given key, get and has return value and presence indicator respectively, support both regular object and null-object.

# toEs6Map
Converts object to ES6 Map with the same keys and values. As the name implies.

# forIn
Iterates over object entries. Use for optimization purposes only. Otherwise just iterate over keys.
